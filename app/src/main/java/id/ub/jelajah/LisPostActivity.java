package id.ub.jelajah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class LisPostActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<DiaryPost> listdiary=new ArrayList<DiaryPost>();
    ImageView back;

    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference getReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lis_post);

        recyclerView=findViewById(R.id.recyclerView);
        back=findViewById(R.id.back);

        ListAdapter adapter= new ListAdapter(this, listdiary);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager((LisPostActivity.this)));

        database=FirebaseDatabase.getInstance();
        auth=FirebaseAuth.getInstance();
        String uid=auth.getCurrentUser().getUid();
        getReference=FirebaseDatabase.getInstance().getReference("Users").child(uid).child("Post");

        getReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot data: snapshot.getChildren()){
                    DiaryPost User=data.getValue(DiaryPost.class);
                    listdiary.add(User);
                }

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LisPostActivity.this, MenuActivity.class));
                finish();
            }
        });
    }
}