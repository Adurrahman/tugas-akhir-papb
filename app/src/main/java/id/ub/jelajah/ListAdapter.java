package id.ub.jelajah;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyviewHolder> {

    Context Context;
    ArrayList<DiaryPost> Diary;

    public ListAdapter(Context context, ArrayList<DiaryPost> diary){
        this.Context=context;
        this.Diary=diary;
    }

    @NonNull
    @Override
    public ListAdapter.MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(Context);
        View view = inflater.inflate(R.layout.list_diary_card, parent, false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.MyviewHolder holder, @SuppressLint("RecyclerView") int position) {

        DiaryPost diaryPost = Diary.get(position);
        holder.isiPost.setText(diaryPost.getIsiPost());
        holder.judulPost.setText(diaryPost.getJudulPost());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Context, ViewPostActivity.class);
                intent.putExtra("data", (Serializable) diaryPost);
                Context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return Diary.size();
    }

    public void delete(int index){

    }

    public class MyviewHolder extends RecyclerView.ViewHolder{
        TextView judulPost, isiPost;
        CardView card;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);
            judulPost=itemView.findViewById(R.id.Judul);
            isiPost=itemView.findViewById(R.id.isipost);
            card=itemView.findViewById(R.id.card);

        }
    }
}
