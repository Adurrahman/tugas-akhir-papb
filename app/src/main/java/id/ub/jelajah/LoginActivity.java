package id.ub.jelajah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {

    private Button BtnMasuk, BtnMasukGoogle, BtnDafatar;
    private EditText Email;
    private EditText Password;

    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference getReference;

    @Override
    protected void onStart() {
        super.onStart();
        if (auth.getCurrentUser()!=null){
            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BtnDafatar=findViewById(R.id.buttondaftar);
        BtnMasuk=findViewById(R.id.buttonmasuk);
        Email=findViewById(R.id.inputemail);
        Password=findViewById(R.id.inputpass);

        database = FirebaseDatabase.getInstance(); // Mendapatkan Instance Firebase Realtime Database
        auth = FirebaseAuth.getInstance(); //Menghubungkan dengan Firebase Authentifikasi

        BtnDafatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, DaftarActivity.class));
            }
        });

        BtnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                login(Email.getText().toString(), Password.getText().toString());
            }
        });
    }

    private void login(String email, String passwaord){
        auth.signInWithEmailAndPassword(email, passwaord)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            startActivity(new Intent(LoginActivity.this, MenuActivity.class));
                            finish();
                        }
                        else
                            Toast.makeText(LoginActivity.this, "Login Gagal", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}