package id.ub.jelajah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MenuActivity extends AppCompatActivity {

    private Button BtnMakePost, BtnReadPost, BtnKeluar;
    private TextView TextUsername;

    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference getReference;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        BtnMakePost=findViewById(R.id.button_buatDiary);
        BtnReadPost=findViewById(R.id.button_bacaDIary);
        BtnKeluar=findViewById(R.id.button_keluar);

        TextUsername=findViewById(R.id.textUsername);

        database=FirebaseDatabase.getInstance();
        auth=FirebaseAuth.getInstance();
        String uid=auth.getCurrentUser().getUid();
        getReference=FirebaseDatabase.getInstance().getReference("Users").child(uid).child("userName");
        getdata();

        BtnKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                startActivity(new Intent(MenuActivity.this, LoginActivity.class));
                finish();
            }
        });

        BtnMakePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, BuatDiaryActivity.class));
            }
        });

        BtnReadPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, LisPostActivity.class));
            }
        });
    }


    private void getdata(){
        getReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String User=snapshot.getValue(String.class);
                TextUsername.setText(User);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}