package id.ub.jelajah;

public class users {
    private String UserName;
    private String UserPass;
    private DiaryPost Post;
    private String Email;
    private String Id_Users;

    public users() {
    }

    public users(String userName, String userPass, String email, String id_Users) {
        this.UserName = userName;
        this.UserPass = userPass;
        this.Email=email;
        this.Id_Users=id_Users;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserPass() {
        return UserPass;
    }

    public void setUserPass(String userPass) {
        UserPass = userPass;
    }

    public DiaryPost getPost() {
        return Post;
    }

    public void setPost(DiaryPost post) {
        Post = post;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getId_Users() {
        return Id_Users;
    }

    public void setId_Users(String id_Users) {
        Id_Users = id_Users;
    }
}
