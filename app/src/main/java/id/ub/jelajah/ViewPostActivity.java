package id.ub.jelajah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class ViewPostActivity extends AppCompatActivity {

    ImageView Back;
    EditText judulpost, isipost;
    TextView Delete;

    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference getReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        Bundle bundle=getIntent().getExtras();
        DiaryPost id=(DiaryPost) bundle.getSerializable("data");

        Delete=findViewById(R.id.Hapuslist);
        Back=findViewById(R.id.backlist);
        judulpost=findViewById(R.id.judulPostList);
        isipost=findViewById(R.id.isiPostList);

        judulpost.setText(id.getJudulPost());
        isipost.setText(id.getIsiPost());


        database=FirebaseDatabase.getInstance();
        auth=FirebaseAuth.getInstance();
        String uid=auth.getCurrentUser().getUid();
        getReference=FirebaseDatabase.getInstance().getReference("Users").child(uid).child("Post");


//
//
//
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DiaryPost diary=new DiaryPost(id.getId_post(), judulpost.getText().toString(),
                        isipost.getText().toString());

                Map<String, Object> DiaryValues = diary.toMap();

                Map<String, Object> userUpdates = new HashMap<>();

                userUpdates.put("/"+id.getId_post(), DiaryValues);
                getReference.updateChildren(userUpdates);
                startActivity(new Intent(ViewPostActivity.this, LisPostActivity.class));
                finish();
            }
        });

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getReference.child(id.getId_post()).removeValue();
                startActivity(new Intent(ViewPostActivity.this, LisPostActivity.class));
                finish();
            }
        });
    }
}