package id.ub.jelajah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DaftarActivity extends AppCompatActivity {

    Button Masuk;
    Button Daftar;
    EditText UserName;
    EditText Email;
    EditText Password;


    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference getReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        Masuk=findViewById(R.id.buttonmasuk);
        Daftar=findViewById(R.id.regis_buttondaftar);
        UserName=findViewById(R.id.inputnama);
        Email=findViewById(R.id.inputemail);
        Password=findViewById(R.id.inputpass);


        database = FirebaseDatabase.getInstance(); // Mendapatkan Instance Firebase Realtime Database
        auth = FirebaseAuth.getInstance(); //Menghubungkan dengan Firebase Authentifikasi

        Masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DaftarActivity.this, LoginActivity.class));
                finish();
            }
        });

        Daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Register(Email.getText().toString(), Password.getText().toString());
            }
        });

    }

    private void Register(String email, String password){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(DaftarActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(DaftarActivity.this, auth.getCurrentUser().getUid(), Toast.LENGTH_SHORT).show();
                            users User= new users(UserName.getText().toString(), Password.getText().toString(),
                                    Email.getText().toString(), auth.getCurrentUser().getUid());

                            database.getReference().child("Users").child(auth.getCurrentUser().getUid()).setValue(User);
                            auth.signOut();
                        }
                        else
                            Toast.makeText(DaftarActivity.this, "Register Gagal", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}