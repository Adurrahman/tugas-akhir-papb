package id.ub.jelajah;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DiaryPost implements DatabaseReference.CompletionListener, Serializable {
    private String Id_post;
    private String JudulPost;
    private String IsiPost;
    private Bitmap GambarPost;

    public DiaryPost() {
    }

    public DiaryPost(String id_post, String judulPost, String isiPost, Bitmap gambarPost) {
        Id_post = id_post;
        JudulPost = judulPost;
        IsiPost = isiPost;
        GambarPost = gambarPost;
    }

    public DiaryPost(String id_post, String judulPost, String isiPost) {
        Id_post = id_post;
        JudulPost = judulPost;
        IsiPost = isiPost;
    }

    public String getId_post() {
        return Id_post;
    }

    public void setId_post(String id_post) {
        Id_post = id_post;
    }

    public String getJudulPost() {
        return JudulPost;
    }

    public void setJudulPost(String judulPost) {
        JudulPost = judulPost;
    }

    public String getIsiPost() {
        return IsiPost;
    }

    public void setIsiPost(String isiPost) {
        IsiPost = isiPost;
    }

    public Bitmap getGambarPost() {
        return GambarPost;
    }

    public void setGambarPost(Bitmap gambarPost) {
        GambarPost = gambarPost;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("Id_post", Id_post);
        result.put("JudulPost", JudulPost);
        result.put("IsiPost", IsiPost);

        return result;
    }

    @Override
    public void onComplete(@Nullable DatabaseError error, @NonNull DatabaseReference ref) {

    }
}
