package id.ub.jelajah;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class BuatDiaryActivity extends AppCompatActivity {

    ImageView back;
    TextView Posting;
    EditText JudulPost, IsiPost;

    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference getReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_diary);

        back=findViewById(R.id.back);
        Posting=findViewById(R.id.posting);
        JudulPost=findViewById(R.id.judulPost);
        IsiPost=findViewById(R.id.isiPost);

        database = FirebaseDatabase.getInstance(); // Mendapatkan Instance Firebase Realtime Database
        auth = FirebaseAuth.getInstance(); //Menghubungkan dengan Firebase Authentifikasi
        String uid=auth.getCurrentUser().getUid().toString();
        getReference=database.getReference().child("Users").child(uid);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BuatDiaryActivity.this, MenuActivity.class));
                finish();
            }
        });

        Posting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String IdPost=database.getReference().push().getKey();
                DiaryPost diary=new DiaryPost(IdPost, JudulPost.getText().toString(),
                        IsiPost.getText().toString());

                Map<String, Object> DiaryValues = diary.toMap();


                Map<String, Object> userUpdates = new HashMap<>();
                userUpdates.put("/Post/"+IdPost, DiaryValues);
                getReference.updateChildren(userUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        startActivity(new Intent(BuatDiaryActivity.this, MenuActivity.class));
                        finish();
                    }
                });
            }
        });
    }
}